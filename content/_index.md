This is the latest iteration of my blog where I put stuff I learn or do
that might be of interest to others, or just so I can link to it from somewhere.

I don't get around to it very often, but here it is. This latest iteration is
powered by [GitLab Pages](https://about.gitlab.com/features/pages/) and [Hugo](https://gohugo.io),
because I like [Markdown](https://daringfireball.net/projects/markdown/).
