---
title: About me
comments: false
---

I'm an engineer, currently working with software in my dayjob, but
I also tool around with hardware in my free time. Mostly, I like figuring out
how things work. This blog is intended to be, among other things, a
place to document some of those things, especially if they might be useful
or ineresting to other people. Or if I just want _something_ to justify the
ridiculous amount of time I spent making charts and diagrams for no reason.
