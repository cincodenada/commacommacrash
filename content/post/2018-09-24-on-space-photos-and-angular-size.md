---
title: On Space Photos and Angular Size
date: 2018-09-24
draft: true
---

A while back I set up a script to download pictures from NASA's excellent
[Astronomy Picture of the Day][] and rotate through them as my wallpaper. That
script will eventually get its own post, but the important part here is that one
day a photo titled ["Moon over Planet Earth"][apodmoon] showed up on my desktop:

![An satellite photo of a cloudy Pacific Northwest, with the arc of the earth's
circumference curving from the lower-left corner to lower-right, and the Moon
looming very large in the upper-left corner of the image](/img/ab_moon_from_geo_orbit_med_res_jan_15_2017_1024.jpg)

My first reaction was that this has to be a composition, which isn't unheard of
-- photoillustrations are sometimes [featured on APOD][photoillustration] --
but they're usually made pretty clear. So I did some digging and found a post
where NASA talked more [about the image][aboutphoto] and the spacecraft that
took the photograph. It's [GOES-16][], formerly known as GOES-R, a weather
satellite whose job is to keep an eye on the weather over the contiguous United
States. The photo above is from a calibration run - apparently the moon is a
known target that is used to calibrate the image sensors on the spacecraft.

I don't know if it's happenstance or if the scientists at the helm saw an
opportunity, but the thing that makes this photo so breathtaking is how close
the moon is to the earth - just over its shoulder, so to speak. It has an extra
personal connection because what little land you can see peeking through the
clouds is the Pacific Northwest of the United States, which is where I've lived
my whole life, and where I gladly call home.

The main thing that's so mindboggling about this photo, of course, is just how
_big_ the moon looks in comparison to the earth. The moon is more than a quarter
of the earth's diameter, but it also orbits between 350-400,000km away - that's
30 earth diameters. In the classic convenient scale model of a basketball and a
tennis ball, the proper distance is 7m - if you put the basketball-earth in the
center of a basketball or tennis court, the orbit of the tennis-moon would fit
nicely between the sidelines of the basketball court, or between the center
service lines of the tennis court. Here's a to-scale image from Wikipedia:

![Scale image of the Earth/Moon system](https://upload.wikimedia.org/wikipedia/commons/thumb/3/3f/Earth-moon-to-scale.svg/800px-Earth-moon-to-scale.svg.png)

So, given how far away the moon is, how does it look so big and close in the
photo? "Close" is easy - it just so happens that from the perspective of
GOES-16, the moon was "just over the shoulder" of Earth - it was just about to
set behind the earth from the perspective of GOES-16, in fact.

But it still looks so _big_! To convince myself it was possible for this to be a
photograph, I actually fired up [Celestia][] and recreated this photograph.
The whole story there is enough for a post of its own, but the short of it is
that if you have Celestia installed you can check out my best reproduction of it
by clicking or copy/pasting [this Celestia URI][celestialink]. If you're a big
Celestia nerd, you can download my [ssc](/stuff/GOES-16.ssc) and optionally
[3ds](/stuff/GOES.3ds) and put them in your `extras` and `models` folders,
respectively. With those installed, you can use [this link][celestiagoes], which
will also follow GOES-16's orbit. In any case, it looked something like this:

[!A 3D rendering of the Moon and Earth that resembles the NASA image
above](/img/GOES-16_moon_reproduction.png)

Not perfect, but close enough to prove the point. So why so big? Well, firstly,
the camera is a pretty "zoomed in", similar to a telephoto lens. This makes for
a narrow field of view (FOV), which reduces the degree to which farther-away
things look smaller, and has the effect of making far away things look bigger
than you're used to. This can have some extreme effects - for instance, [another
APOD entry](apodbigmoon) features a video that seems to show a gargantuan moon
setting over a ridge with people on it. The reason the sizes are so different
from what we're used to seeing is that the video was taken from 16km away,
through a telescope. Similarly, GOES-16 is pretty far away from Earth, which is
the other key piece. It's in geosynchronous orbit, which is the highest of the
common satellite orbits by far at over 35,000km above earth's surface. By
comparison, the ISS orbits in low-earth orbit about 400km up. GOES-16 is about
1/10th of the way out to the Moon, which doesn't seem like a lot, but is enough
to put a good bit of distance between it and the earth.

The thing that spurred me to write this post is a [tweet][fengyuntweet]
(retweeted into my timeline by the excellent [@Rainmaker1973][rainmaker])
featuring a recent photo from Fengyun-4A, a Chinese weather satellite that seems
to be basically be their version of US GOES satellites. It's also pretty
stunning:

![A full-frame photo of a half-illuminated earth, with a similarly-illuminated
moon in the upper-left hand corner, visally 1/16 of the size of
Earth.](/img/earth_moon_fengyun.png)

It, too, was met with some skepticism, so I wanted to do some math about angular
sizes and the like to see if the math works out, and that's more fun to do if
I'm putting it in a blog post. So I of course first spun up an entirely new blog
running on a static site generator, cause that's the cool thing to do these
days, and then drafted this post.

So! I'll start with the Fengyun photo, since it has the full frame of earth in
it and thus is easier to measure things on. Looking at pixel sizes (I use the
GIMP's measure tool), the earth is about 600px in diameter. Given what we know
about the satellite's orbit, we should be able to calculate how big the moon
should look and see if it matches up. Here's a sketchy diagram I whipped up in
Inkscape:

I'm gonna play fast and loose with my vague understanding of significant figures
here, because we're just estimating anyway, and handwaving is fun. So, we know
that the satellite is orbiting at about 35,000km above the surface - add the
earth's radius to that and you have d<sub>e</sub> of 41,000km. r<sub>e</sub>, of
course, is about 6400km. From that, we can calculate that the angular size of
earth from the satellite's point of view:

$$ sin(\theta_e) = \frac{r_e}{d_e}$$
$$\theta_e = sin^{-1}(\frac{r_e}{d_e})$$
$$\theta_e = sin^{-1}(\frac{6400km}{41000km})$$
$$\theta_e = sin^{-1}(0.156)$$
$$\theta_e = 9.0\degree $$

That's for half of earth, since we used the radius - that's 18 degrees for the
whole thing.

We don't know how far away the moon is exactly because it depends on the
position of the moon and the satellite relative to earth, but we can get some
ranges.  Adding in the earth's radius, the satellite is about 41,000km from the
earth's center. When the satellite is between the earth and moon, it's at its
closest `350,000km - 41,000km = 309,000km` away, and when it's opposite the
earth from the moon, it's at its farthest `400,000km + 41,000km = 441,000km`
away. We can run both numbers, with the moon's radius of 1700km:

$$\theta_e = sin^{-1}(1700km/310000km)$$
$$\theta_e = $$0.31\degree$$
$$\theta_e = sin^{-1}(1700km/440000km)$$
$$\theta_e = $$0.22\degree$$

Multiply it by two, and we get from 0.44 - 0.62 degrees.

To get to pixels, we need to pick some distance to put the plane that we're
projecting the photo onto. I pick one that intersects with earth, because that
makes the math easy. 



[Astronomy Picture of the Day][]
[apodmoon]: https://apod.nasa.gov/apod/ap170126.html
[aboutphoto]: https://www.nasa.gov/feature/goddard/2017/goes-16-sends-first-images-to-earth
[photoillustration]: https://apod.nasa.gov/apod/ap060131.html
[GOES-16]: https://en.wikipedia.org/wiki/GOES-16
[celestialink]: cel://Follow/Sol:Earth:Moon/2017-01-15T20:38:06.38882?x=gNDgxJ5AuQo&y=Ky0JAXkIGQ&z=QOf3Xd5JJQQ&ow=0.812659&ox=0.114398&oy=-0.551994&oz=0.14765&select=Sol:Earth:Moon&fov=1.68117&ts=-4&ltd=0&p=1&rf=317351&lm=354&tsrc=0&ver=3
[celestiagoes]: cel://Follow/Sol:Earth:GOES-16/2017-01-15T20:38:06.38882?x=y08offr//////////////w&y=I/JMOP3//////////////w&z=Imf8E/3//////////////w&ow=0.812659&ox=0.114398&oy=-0.551994&oz=0.14765&select=Sol&fov=1.68117&ts=-4&ltd=0&p=1&rf=317351&lm=354&tsrc=0&ver=3
[apodbigmoon]: https://apod.nasa.gov/apod/ap180604.html
