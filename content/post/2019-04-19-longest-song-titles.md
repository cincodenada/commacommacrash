---
title: A post investigating the question of which popular band has
the consistently longest song titles, inspired by a one-off joke I made about a
video game
date: 2019-04-19
draft: true
---

This morning at work, someone posted an ad for an old Atari game called
Spinnaker in our #gaming sidechannel:

![Box art for the game "In Search of the Most Amazing Thing" by Tom Snyder
Productions, published by Spinnaker Software in 1983. The box art features a
futuristic hot air balloon](/img/isotmat.jpg)

It was quickly pointed out that, improbably, the name of the game was not
Spinnaker, that was the publisher - the game was actually titled "In Search of
the Most Amazing Thing", and the picture was not an ad, but the box art.

I made a throwaway joke about Fall Out Boy naming the game, and then doubted
myself - who is the best posterband for unnecessarily long song titles? I'm what
I would call a casual fan of both Fall Out Boy and Panic! at the Disco, probably
the most prominent contenders, but which one was a better punchline? And are
there better ones?

Instead of just letting it go after my joke was sucessful and got the requisite
reacji, I got to thinking, as I often do...hey, this data is out there, it
exists, I could make some empirical analysis of this question. And, of course,
some charts.

So, I got to work. Of course my first stop was the place I get all of the
metadata to meticulously tag my own music collection:
[MusicBrainz](https://musicbrainz.org/), the Open Music Encyclopedia. I quickly
found their freely-available [database
dumps](https://musicbrainz.org/doc/MusicBrainz_Database), and set my computer
downloading the latest one for later persual.

Now I am home and have the data at my fingertips, and what better to do on a
free Friday night? The database dump is intended to be loaded into an actual
database, which I might do eventually, but for now my needs are pretty simple.
Fortunately, the tables are all just represented as TSV's, so it's easy to start
pulling out data without anything fancy.

I started noodling around making some assumptions about columns, and then found
the [schema description](https://musicbrainz.org/doc/MusicBrainz_Database/Schema) that
laid it all out, and started pulling things into R. Here's what I ended up with:

// TODO: R script

Pretty standard stuff, I had to tell it that the columns aren't quoted at all,
because it was breaking on releases such as `"Bacon Fat" Recordings from
1956-1959` that had quotes in them - I assume the titles never contain tabs, so
no quoting is needed. The `if(!exists('artists')) { ... }` wrapper is a
technique I use so that I don't have to re-load the data every time when I want
to re-load the script - I can keep a session up, and if I need to reload a
table, I can just `rm(artists)`, but otherwise I get the new analysis without
reloading the data from disk. Neato! Did some basic checks, and we're in
business:

```
> nrow(link)
[1] 601692
> nrow(artists)
[1] 1486471
> nrow(recordings)
[1] 20370484
> nrow(links)
[1] 2683686
```

Time to link things up! This took a while to figure out, first I
mistakenly loaded up the `releases` table instead of the `recordings`, and then
faddled about with the `l_*` tables for a bit before realizing it would be way
better to just use the `artist_credit_name` table instead, and getting all the
merges right and such was a lot of trial and error. I started with a sample of
the first 10000 recordings so I could get everything aligned up right, and
here's what I ended up with:

```R
if(!exists('joined')) {
  joined = recordings %>% head(10000) %>%
    merge(links %>% filter(position==0), by=c('artist_credit'), suffixes=c('.recording','.c')) %>%
    merge(artists, by.x='artist', by.y='id',suffixes=c('.artist','.l'))
}
```

In retrospect, it might have been worth it tossing this into an actual Postgres
database and just querying out of there, but, where's the fun in that? And lord
knows how long it would take to get all that straightened out, and loading TSVs
into R is a much more known quantity.

Now that I've got all the data together, time to pull out what we want and make
a chart of it! That part was pretty straightforward. Get a list of unique
artist/song pairs and count the number of letters in the song title. Then group
that dataframe by artist, count the number of songs and the median of the song
title length, and badda boom, badda bing, we can toss it into a chart! Because
of how they merged, `name` is the artist name, and `name.recording` is the
recording name.

```R
songs = unique(joined %>% select(name, name.recording)) %>%
  mutate(chars=str_length(name.recording))

avg = songs %>%
  group_by(name) %>%
  summarize(median=median(chars), count=n())

p = ggplot(avg, aes(x=median, y=count, label=name)) +
  geom_point() + 
  geom_text(hjust=0, nudge_x=0.5)
```

From the quick chart, a couple things stand out: the "[unknown]" artist should
be filtered out, which reminds me so should the "Various Artists" artist. There
are actually a few "Various Artists" artists, and only one of them is the
canonical one. We could be sloppy, but once we have the full list, we can also
just sort to see who has the most recordings attributed to them, and the top
will of course be Various Artists:

```R
> avg %>% arrange(-count) %>% head(10)
# A tibble: 10 x 3
               name.artist median count
                     <chr>  <dbl> <int>
 1               [unknown]     20 88713
 2             [no artist]     30 18044
 3           J. K. Rowling     33 15736
 4             Sound Ideas     48 13635
 5  [language instruction]     24 13007
 6          Giuseppe Verdi     46 12674
 7         Ennio Morricone     22 10559
 8  George Frideric Handel     49  9709
 9             The Beatles     26  9709
10 Wolfgang Amadeus Mozart     53  9258
```

The first time I tried to run it on the full set, my computer exhausted its 16GB
of RAM and started thrashing trying to swap things to disk. As I type it's on a
second attempt where I only selected the columns I need for the joined table,
and clean up the source tables afterwards, and it's behaving better.


